# Utilities to configure hosts to use the FUSS Mail Gateway as mail smarthost

## Configuration

Clone this repository first:
```bash
git clone https://gitlab.fuss.bz.it/fuss-team/fuss-mg-scripts.git
cd fuss-mg-scripts
```

Run the correct playbook, for FUSS Server or Proxmox host:

For the FUSS Server:
```bash
ansible-playbook -i 192.168.1.1, configure-fuss.yaml
```

For Proxmox:
```bash
ansible-playbook -i 192.168.1.1, configure-proxmox.yaml
```


TIP: if you don't have SSH keys installed on the host, you can
use the `--ask-pass` ansible option.


## Testing

For both the Proxmox host and the FUSS Server you can test with 

```bash
echo "Test mail from postfix" | mail -s "Test Postfix" test@email.co
```

making sure `bsd-mailx` or another Debian mailer package is installed.

On Proxmox, you can also test with

```bash
echo "test" | /usr/bin/pvemailforward
```

but you'll need to change the `root` user email address
before.
